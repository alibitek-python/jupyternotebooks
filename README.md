# Jupyter Notebooks

## Usage
```
jupyter --help
usage: jupyter [-h] [--version] [--config-dir] [--data-dir] [--runtime-dir]
               [--paths] [--json]
               [subcommand]

Jupyter: Interactive Computing

positional arguments:
  subcommand     the subcommand to launch

optional arguments:
  -h, --help     show this help message and exit
  --version      show the jupyter command's version and exit
  --config-dir   show Jupyter config dir
  --data-dir     show Jupyter data dir
  --runtime-dir  show Jupyter runtime dir
  --paths        show all Jupyter paths. Add --json for machine-readable
                 format.
  --json         output paths as machine-readable json

Available subcommands: bundlerextension kernel kernelspec lab labextension
labhub migrate nbconvert nbextension notebook run serverextension troubleshoot
trust

Setting up a Jupyter Lab remote server: https://agent-jay.github.io/2018/03/jupyterserver/
Running a notebook server: https://jupyter-notebook.readthedocs.io/en/stable/public_server.html
```

### How to share Jupyter notebook on a private internal network?
.1 Generate notebook configuration: `jupyter notebook --generate-config`

.2 Customize configuration  
Config files are stored in: 
- $HOME/.jupyter/jupyter_notebook_config.py
- $HOME/.jupyter/jupyter_notebook_config.json has higher priority

Set configuration in `$HOME/.jupyter/jupyter_notebook_config.py`:
```
## The IP address the notebook server will listen on.
c.NotebookApp.ip = '*'

## The port the notebook server will listen on.
c.NotebookApp.port = 8888

## Whether to open in a browser after starting. The specific browser used is
#  platform dependent and determined by the python standard library `webbrowser`
#  module, unless it is overridden using the --browser (NotebookApp.browser)
#  configuration option.
c.NotebookApp.open_browser = True
```
.3 Set a password: `jupyter notebook password`  
.4 Run the notebook server: `jupyter notebook`  

You might also need to open a port in the firewall for other machines to access the port where Jupyer Notebook server is listening:   
`sudo ufw allow from 192.168.1.0/24 to any port 8888 proto tcp`

### How to share Jupyter notebook on a public network?
Same as above, with the addition that you need to:  
.1 Generate a self-signed certificate for SSL encrypted communication: `openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout mykey.key -out mycert.pem`  
.2 Set configuration in `$HOME/.jupyter/jupyter_notebook_config.py`:  
```
c.NotebookApp.certfile = u'/absolute/path/to/your/certificate/mycert.pem'
c.NotebookApp.keyfile = u'/absolute/path/to/your/certificate/mykey.key'
```

## JupyterHub
- https://jupyterhub.readthedocs.io/en/latest/index.html
- https://github.com/jupyterhub/jupyterhub
- https://github.com/jupyterhub/jupyterhub-tutorial

## NumericalAnalysisNotebook
- [Gaussian elimination for solving systems of linear equations](https://en.wikipedia.org/wiki/Gaussian_elimination)
- [Doolittle decomposition](https://en.wikipedia.org/wiki/LU_decomposition#Doolittle_algorithm)
- [Cholesky decomposition](https://en.wikipedia.org/wiki/Cholesky_decomposition)
- [Newton's divided differences interpolation polynomial](https://en.wikipedia.org/wiki/Newton_polynomial)
- [Lagrange interpolation](https://en.wikipedia.org/wiki/Lagrange_polynomial)
- [Bisection method](https://en.wikipedia.org/wiki/Bisection_method)
- [Secant method](https://en.wikipedia.org/wiki/Secant_method)
- [Simpson's rule](https://en.wikipedia.org/wiki/Simpson%27s_rule)
- [Trapezoidal rule](https://en.wikipedia.org/wiki/Trapezoidal_rule)